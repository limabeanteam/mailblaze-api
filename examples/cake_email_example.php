<?php 

/*  -----------OUTLINE OF WHAT TO DO-------------

These are the steps you need to take to use the MailBlaze Transactional API in Cake phpcredits()

1. Get the APU publicKey and Url from the clients account. 
2. Add this array with the above values in the configuration file in Cake (app.php). 
		
		'MailBlazeApi' => [
            'apiUrl'        => 'https://chi.mailblaze.com/api/index.php',
            'publicKey'     => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        ]

3. Copy the MailBlazeApiTransport.php file in to the \src\Mailer\Transport\ directory
4. Use the example as outlined below to use the Mail Blaze API as the email transport mechanism 


*/


namespace App\Shell;

use Cake\Mailer\Email;
use App\Mailer\Transport\MailBlazeApiTransport;

class LookupStatusShell extends Shell
{


	public function testEmailTransport()
    {            
        $email = new Email();
        $transport = new MailBlazeApiTransport();       

        $email->setTransport($transport);
        $email->from([Configure::read("ShellMail.Support.from") => 'Mail Blaze Test Transport'])
            ->to("spiro@mailblaze.com")            
            ->subject('Test Transport')            
            ->template('processing', 'support')
            ->emailFormat('both')
            ->set([
                'campaignName' => "testme",
                'customerEmail' => "not@important",
                'startedAt' => "2018-12-05 12:00:00",
                'titleOne' => 'Transport - ',
                'titleTwo' => 'Test'
            ])            
            //The send at needs to be formatted as follows: YYYY-MM-DD HH:MM:SS
            //->setHeaders(["X-Send-At" => "2018-12-06 12:00:00"])
            ->send();

    }
}

?>