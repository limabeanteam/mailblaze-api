<?php
use MailBlazeApi\Endpoint\ListFields;

require_once dirname(__FILE__) . '/setup.php';

// CREATE THE ENDPOINT
$endpoint = new ListFields();

/*===================================================================================*/

// GET ALL ITEMS
$response = $endpoint->getFields('LIST-UNIQUE-ID');

// DISPLAY RESPONSE
echo '<pre>';
print_r($response->body);
echo '</pre>';