<?php
/*NOTE:::: This transport class is for versions of Cake PHP greater than 3.4*/


namespace App\Mailer\Transport;

use Cake\Core\Configure;
use Cake\Mailer\AbstractTransport;
use Cake\Mailer\Email;
use MailBlazeApi\Base;
use MailBlazeApi\Config;
use MailBlazeApi\Endpoint\TransactionalEmails;
use Cake\Log\Log;

class MailBlazeApiTransport extends AbstractTransport
{

	/**
     * Default config for this class
     *
     * @var array
     */
    protected $_defaultConfig = [
        'apiUrl'        => 'https://chi.mailblaze.com/api/index.php',
        'publicKey'     => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    ];


    /**
     * Send mail
     *
     * @param \Cake\Mailer\Email $email Cake Email
     * @return array
     */
    public function send(Email $email)
    {
        $eol = PHP_EOL;

        //Set the configuration for the emails
        $config = new Config(Configure::read('MailBlazeApi'));
        // now inject the configuration and we are ready to make api calls
        Base::setConfig($config);        

        //Get all the email information to pass. 
        $subject = (!empty($email->getSubject())) ? str_replace(["\r", "\n"], '', $email->getSubject()) : "";
        $to = (!empty($email->getTo())) ? str_replace(["\r", "\n"], '', key($email->getTo())) : "";
        $to_name = (!empty($email->getTo())) ? str_replace(["\r", "\n"], '', $email->getTo()[$to]) : "";
        $from = (!empty($email->getFrom())) ? str_replace(["\r", "\n"], '', key($email->getFrom())) : "";
        $from_name = (!empty($email->getFrom())) ? str_replace(["\r", "\n"], '', $email->getFrom()[$from]) : "";
        $reply = (!empty($email->getReplyTo())) ? str_replace(["\r", "\n"], '', key($email->getReplyTo())) : "";
        $reply_name = (!empty($email->getReplyTo())) ? str_replace(["\r", "\n"], '', $email->getReplyTo()[$reply]) : "";

        $headers = $email->getHeaders(['X-Send-At']);
        $SendAt = (!empty($headers['X-Send-At'])) ? $headers['X-Send-At'] : "";        
        foreach ($headers as $key => $header) {
            $headers[$key] = str_replace(["\r", "\n"], '', $header);
        }
        $headers = $this->_headersToString($headers, $eol);
          
        $plain_text_message = $email->message('text');
        $html_message = $email->message('html');

        $params = isset($this->_config['additionalParameters']) ? $this->_config['additionalParameters'] : null;        
        
        $endpoint = new TransactionalEmails();
 
        $response = $endpoint->create([  
            'to_email' => $to,
            'to_name' => $to_name,          
            'from_email' => $from,
            'from_name' => $from_name,
            'reply_to_email' => $reply,
            'reply_to_name' => $reply_name,            
            'subject' => $subject,
            'body' => $html_message,
            'plain_text' => $plain_text_message,
            'send_at' => $SendAt
        ]);

        if ($response->body['status'] == 'error'){
            Log::write('error', 'Transport class:: There is an error writing to the Mail Blaze Api. The description of the error is: '. $response->body['error']);
        }
        
        $headers .= $eol . 'To: ' . $to;
        $headers .= $eol . 'Subject: ' . $subject;



        return ['headers' => $headers, 'message' => $html_message, 'status' => $response->body['status']];
    }

}
?>