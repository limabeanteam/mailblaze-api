<?php
namespace MailBlazeApi\Cache;

class Dummy extends CacheAbstract {
    /**
     * Cache data by given key.
     *
     * This method implements {@link MailBlazeApi\Cache\CacheAbstract::set()}.
     *
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function set ($key, $value) {
        return true;
    }

    /**
     * Get cached data by given key.
     *
     * This method implements {@link MailBlazeApi\Cache\CacheAbstract::get()}.
     *
     * @param string $key
     * @return mixed
     */
    public function get ($key) {
        return null;
    }

    /**
     * Delete cached data by given key.
     *
     * This method implements {@link MailBlazeApi\Cache\CacheAbstract::delete()}.
     *
     * @param string $key
     * @return bool
     */
    public function delete ($key) {
        return true;
    }

    /**
     * Delete all cached data.
     *
     * This method implements {@link MailBlazeApi\Cache\CacheAbstract::flush()}.
     *
     * @return bool
     */
    public function flush () {
        return true;
    }
}