<?php
namespace MailBlazeApi\Cache;

use MailBlazeApi\Base;

abstract class CacheAbstract extends Base {
    /**
     * @var array keeps a history of loaded keys for easier and faster reference
     */
    protected $_loaded = array();

    /**
     * Set data into the cache
     *
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    abstract public function set ($key, $value);

    /**
     * Get data from the cache
     *
     * @param string $key
     * @return mixed
     */
    abstract public function get ($key);

    /**
     * Delete data from cache
     *
     * @param string $key
     * @return bool
     */
    abstract public function delete ($key);

    /**
     * Delete all data from cache
     *
     * @return bool
     */
    abstract public function flush ();
}