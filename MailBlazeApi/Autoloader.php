<?php
namespace MailBlazeApi;
/**
 * The MailBlazeApi Autoloader class.
 *
 * From within a Yii Application, you would load this as:
 *
 * <pre>
 * require_once(Yii::getPathOfAlias('application.vendors.MailBlazeApi.Autoloader').'.php');
 * Yii::registerAutoloader(array('MailBlazeApi_Autoloader', 'autoloader'), true);
 * </pre>
 *
 * Alternatively you can:
 * <pre>
 * require_once('Path/To/MailBlazeApi/Autoloader.php');
 * MailBlazeApi_Autoloader::register();
 * </pre>
 *
 */
class Autoloader
{
    /**
     * The registrable autoloader
     *
     * @param string $class
     */
    public static function autoloader($class)
    {
        if (strpos($class, 'MailBlazeApi') === 0) {
            $className = str_replace('\\', '/', $class);
            $className = substr($className, 12);
            if (is_file($classFile = dirname(__FILE__) . '/'. $className.'.php')) {
                require_once($classFile);
            }
        }
    }

    /**
     * Registers the MailBlazeApi_Autoloader::autoloader()
     */
    public static function register()
    {
        spl_autoload_register(array('MailBlazeApi\Autoloader', 'autoloader'));
    }
}