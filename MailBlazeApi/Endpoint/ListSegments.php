<?php

namespace MailBlazeApi\Endpoint;

use MailBlazeApi\Base;
use MailBlazeApi\Http\Client;
use MailBlazeApi\Http\Response;

class ListSegments extends Base
{
    /**
     * Get segments from a certain mail list
     *
     * Note, the results returned by this endpoint can be cached.
     *
     * @param string $listUid
     * @param integer $page
     * @param integer $perPage
     * @return Response
     */
    public function getSegments($listUid, $page = 1, $perPage = 10)
    {
        $client = new Client(array(
            'method'      => Client::METHOD_GET,
            'url'         => $this->config->getApiUrl(sprintf('lists/%s/segments', $listUid)),
            'paramsGet'   => array(
                'page'     => (int) $page,
                'per_page' => (int) $perPage
            ),
            'enableCache' => true,
        ));

        return $response = $client->request();
    }
}
