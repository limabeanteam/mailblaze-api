<?php

namespace MailBlazeApi\Endpoint;

use MailBlazeApi\Base;
use MailBlazeApi\Http\Client;
use MailBlazeApi\Http\Response;

class ListFields extends Base
{
    /**
     * Get fields from a certain mail list
     *
     * Note, the results returned by this endpoint can be cached.
     *
     * @param string $listUid
     * @return Response
     */
    public function getFields($listUid)
    {
        $client = new Client(array(
            'method'      => Client::METHOD_GET,
            'url'         => $this->config->getApiUrl(sprintf('lists/%s/fields', $listUid)),
            'paramsGet'   => array(),
            'enableCache' => true,
        ));

        return $response = $client->request();
    }
}
