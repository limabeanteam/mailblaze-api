<?php

namespace MailBlazeApi\Endpoint;

use MailBlazeApi\Base;
use MailBlazeApi\Http\Client;
use MailBlazeApi\Http\Response;

class Countries extends Base
{
    /**
     * Get all available countries
     *
     * Note, the results returned by this endpoint can be cached.
     *
     * @param integer $page
     * @param integer $perPage
     * @return Response
     */
    public function getCountries($page = 1, $perPage = 10)
    {
        $client = new Client(array(
            'method'      => Client::METHOD_GET,
            'url'         => $this->config->getApiUrl('countries'),
            'paramsGet'   => array(
                'page'     => (int) $page,
                'per_page' => (int) $perPage
            ),
            'enableCache' => true,
        ));

        return $response = $client->request();
    }

    /**
     * Get all available country zones
     *
     * Note, the results returned by this endpoint can be cached.
     *
     * @param integer $countryId
     * @param integer $page
     * @param integer $perPage
     * @return Response
     */
    public function getZones($countryId, $page = 1, $perPage = 10)
    {
        $client = new Client(array(
            'method'      => Client::METHOD_GET,
            'url'         => $this->config->getApiUrl(sprintf('countries/%d/zones', $countryId)),
            'paramsGet'   => array(
                'page'     => (int) $page,
                'per_page' => (int) $perPage
            ),
            'enableCache' => true,
        ));

        return $response = $client->request();
    }
}
