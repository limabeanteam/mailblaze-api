<?php

namespace MailBlazeApi\Endpoint;

use MailBlazeApi\Base;
use MailBlazeApi\Http\Client;
use MailBlazeApi\Http\Response;

use Cake\Http\Client as CakeClient;

class TransactionalEmails extends Base
{
    /**
     * Get all transactional emails of the current customer
     *
     * Note, the results returned by this endpoint can be cached.
     *
     * @param integer $page
     * @param integer $perPage
     * @return Response
     */
    public function getEmails($page = 1, $perPage = 10)
    {
        $client = new Client(array(
            'method'      => Client::METHOD_GET,
            'url'         => $this->config->getApiUrl('transactional'),
            'paramsGet'   => array(
                'page'     => (int) $page,
            ),
            'enableCache' => true,
        ));

        return $response = $client->request();
    }

    /**
     * Get one transactional email
     *
     * Note, the results returned by this endpoint can be cached.
     *
     * @param string $emailUid
     * @return Response
     */
    public function getEmail($emailUid)
    {
        $client = new Client(array(
            'method'      => Client::METHOD_GET,
            'url'         => $this->config->getApiUrl(sprintf('transactional/%s', (string) $emailUid)),
            'paramsGet'   => array(),
            'enableCache' => true,
        ));

        return $response = $client->request();
    }

    /**
     * Create a new transactional email
     *
     * @param array $data
     * @return Response
     */
    public function create(array $data)
    {

        if (!empty($data['body'])) {
            $data['body'] = base64_encode($data['body']);
        }

        if (!empty($data['plain_text'])) {
            $data['plain_text'] = base64_encode($data['plain_text']);
        }

        $client = new CakeClient();

        return $response = $client->post(
            $this->config->getApiUrl('transactional'),
            json_encode($data),
            [
                'type' => 'json',
                'headers' => [
                    'authorization' => $this->config->publicKey
                ]
            ]
        );
    }

    /**
     * Delete existing transactional email
     *
     * @param string $emailUid
     * @return Response
     */
    /*public function delete ($emailUid) {
        $client = new Client(array(
            'method' => Client::METHOD_DELETE,
            'url'    => $this->config->getApiUrl(sprintf('transactional-emails/%s', $emailUid)),
        ));

        return $response = $client->request();
    }*/
}
