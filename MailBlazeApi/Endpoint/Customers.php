<?php

namespace MailBlazeApi\Endpoint;

use MailBlazeApi\Base;
use MailBlazeApi\Http\Client;
use MailBlazeApi\Http\Response;

class Customers extends Base
{
    /**
     * Create a new mail list for the customer
     *
     * The $data param must contain following indexed arrays:
     * -> customer
     * -> company
     *
     * @param array $data
     * @return Response
     */
    public function create(array $data)
    {
        if (isset($data['customer']['password'])) {
            $data['customer']['confirm_password'] = $data['customer']['password'];
        }

        if (isset($data['customer']['email'])) {
            $data['customer']['confirm_email'] = $data['customer']['email'];
        }

        if (empty($data['customer']['timezone'])) {
            $data['customer']['timezone'] = 'UTC';
        }

        print_r($data);

        $client = new Client(array(
            'method'     => Client::METHOD_POST,
            'url'        => $this->config->getApiUrl(),
            'paramsPost' => $data,
        ));

        return $response = $client->request();
    }
}
