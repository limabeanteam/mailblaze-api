mailblaze-php-sdk
================

mailblaze-php-sdk  

Please see the examples folder for usage and available endpoints.

In order to use this SDK you will need to have an active Mail Blaze account. You can register for an account here: https://www.mailblaze.co.za. You will need a public Key to access the API. 